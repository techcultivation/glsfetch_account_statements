import owncloud

oc = owncloud.Client('url')

oc.login('user', 'password')

oc.mkdir('testdir') #

oc.put_file('testdir/testfile.txt', 'testfile.txt')

link_info = oc.share_file_with_link('testdir/testfile.txt') #

print "Here is your link: " + link_info.get_link() #

